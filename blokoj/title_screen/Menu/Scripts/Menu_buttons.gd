extends Node

var load_scene : String

func _ready():
#	$Menu/"Start Game".grab_focus()
	pass
	
func _on_Account_pressed():
	get_tree().change_scene('res://blokoj/title_screen/Menu/scene/Account.tscn')

func _on_Laborcenter_pressed():
	get_tree().change_scene('res://blokoj/title_screen/Menu/scene/Laborcenter.tscn')

func _on_Options_pressed():
	get_tree().change_scene('res://blokoj/title_screen/Menu/scene/Options.tscn')

func _on_Task_pressed():
	get_tree().change_scene('res://blokoj/title_screen/Menu/scene/Tasks.tscn')

func _on_Exit_pressed():
	get_tree().change_scene('res://blokoj/title_screen/Menu/scene/Exit.tscn')


func _on_Panelo_pressed():
	get_tree().change_scene('res://blokoj/title_screen/Menu/scene/Panelo.tscn')
