extends HTTPRequest

func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
	var parsed_resp = parse_json(resp)
	
	Global.token = parsed_resp['data']['ensaluti']['token']
	Global.csrfToken = parsed_resp['data']['ensaluti']['csrfToken']
	Global.status = parsed_resp['data']['ensaluti']['status']
	
	var message = parsed_resp['data']['ensaluti']['message']
	if message == 'Неверный логин или пароль':
		message = tr("wrong login or password")
		$'../../message'.set_mytext(message)
		
	if Global.status:
		Global.scene('root')
		
